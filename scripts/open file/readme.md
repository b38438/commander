- open file with application

```
open ~/Desktop
```

- open file with specified application

```
open -a "Terminal" ~/Desktop/demo.txt
```